$(document).ready(function () {

//Certificate modal

    var modal = '<div id="modal" class="modal"><span class="js-modal-close" onclick="modalClose($(this))">&#215;</span><img src="" alt="modal-image"></div>';

    $('#certificate-img').on('click', function () {
        var tempUrl = $(this).find('img').attr('src');
        $('body').prepend(modal);
        $('.modal').find('img').attr('src', tempUrl);
        $('body, html').addClass('frozen');
    });

    //Burger

    var burger = $('#burger');
    burger.on('click', function () {
        $(this).find('.burger-hidden-content').addClass('opened');
    });
    burger.on('click', '.icon-close', function (e) {
        e.stopPropagation();
        burger.find('.burger-hidden-content').removeClass('opened');
    });

    //ScrollTop

    checkTopScroll();
    $(window).on('scroll, resize', function () {
        checkTopScroll();
    });

    //Functions

    function modalClose(obj) {
        $('#modal').remove();
        $('body, html').removeClass('frozen');
    }

    function checkTopScroll() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop,
            height = $('header.main').height(),
            headerMenu = $('header.menu');

        if (scrolled >= height && $(window).width() <= 960) {
            headerMenu.addClass('fixed');
            $('body').css('padding-top', headerMenu.height());
        } else {
            headerMenu.removeClass('fixed');
            $('body').css('padding-top', '0');
        }
    }

});
